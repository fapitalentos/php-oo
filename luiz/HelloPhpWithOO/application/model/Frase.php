<?php
/**
 * Created by PhpStorm.
 * User: LuizMoura
 * Date: 21/12/2018
 * Time: 17:37
 */

class Frase
{
    private $frase = 'Hello World!';

    /**
     * @return string
     */
    public function getFrase()
    {
        return $this->frase;
    }

    /**
     * @param string $frase
     */
    public function setFrase($frase)
    {
        $this->frase = $frase;
    }


}