<?php
/**
 * Created by PhpStorm.
 * User: Kevin
 * Date: 27/12/2018
 * Time: 14:28
 */

class Produto
{

    private $descricao;
    private $preco;

    /**
     * @return mixed
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * @param mixed $descricao
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    /**
     * @return mixed
     */
    public function getPreco()
    {
        return $this->preco;
    }

    /**
     * @param mixed $preco
     */
    public function setPreco($preco)
    {
        $this->preco = $preco;
    }





}