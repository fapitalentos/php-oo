<?php

class Fabricante{

    private $nome;

        public function __construct($nome)
        {
            $this->nome = $nome;
        }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

}



class Produto{
	
	private $descricao;
    private $preco;
    private $fabricante;

    public function __construct($descricao, $preco,Fabricante $fabricante)
    {
        $this->descricao = $descricao;
        $this->preco = $preco;
        $this->fabricante = $fabricante;
    }

    public function getDetalhes(){
        return "O produto {$this->descricao} custa {$this->preco} reais. Fabrinca: {$this->fabricante->getNome()}";
	}

	
}



$fabricante1 = new Fabricante('Editora B');
$produto1 = new Produto('Livro',50, $fabricante1);


//var_dump($produto1);

echo  $produto1->getDetalhes();
?>