<?php
/**
 * Created by PhpStorm.
 * User: Robson Manfroi
 * Date: 25/12/2018
 * Time: 10:10
 */

class Pessoa {

    public $nome; // atributo

    public function falar(){ // método

        return "O meu nome é " . $this->nome;
    }
}

$name = new Pessoa();
$name->nome = "Robson Manfroi";
echo $name->falar();