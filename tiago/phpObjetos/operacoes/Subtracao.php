<?php
/**
 * Created by PhpStorm.
 * User: Archibald
 * Date: 20/12/2018
 * Time: 01:21
 */

namespace operacoes;

class Subtracao
{
    public function calculo($n1, $n2)
    {
        return $n1 - $n2;
    }
}