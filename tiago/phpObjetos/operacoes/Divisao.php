<?php
/**
 * Created by PhpStorm.
 * User: Archibald
 * Date: 20/12/2018
 * Time: 01:22
 */

namespace operacoes;

class Divisao
{
    public function calculo($n1, $n2)
    {
        if ($n2 != 0) {
            return $n1 / $n2;
        } else {
            echo "<p>Um número não pode ser divisível por 0</p>";
        }
    }
}