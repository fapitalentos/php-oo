<?php
/**
 * Created by PhpStorm.
 * User: Archibald
 * Date: 20/12/2018
 * Time: 01:17
 */

namespace phpObjetos;

interface Calculo
{
    function calculo($n1, $n2);
}