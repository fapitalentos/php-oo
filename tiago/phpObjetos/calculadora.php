<?php
/**
 * Created by PhpStorm.
 * User: Archibald
 * Date: 20/12/2018
 * Time: 01:30
 */

use operacoes\Divisao;
use operacoes\Multiplicacao;
use operacoes\Soma;
use operacoes\Subtracao;

require_once "operacoes/Soma.php";
require_once "operacoes/Subtracao.php";
require_once "operacoes/Multiplicacao.php";
require_once "operacoes/Divisao.php";

if ($_POST) {
    $tipoConta = $_POST['tipoConta'];
    $n1 = $_POST['n1'];
    $n2 = $_POST['n2'];

    switch ($tipoConta) {
        case '+':
            $calculo = new Soma();
            $resultado = $calculo->calculo($n1, $n2);
            echo "O resultado da soma é: " . $resultado;
            break;
        case '-':
            $calculo = new Subtracao();
            $resultado = $calculo->calculo($n1, $n2);
            echo "O resultado da subtração é: " . $resultado;
            break;
        case '*':
            $calculo = new Multiplicacao();
            $resultado = $calculo->calculo($n1, $n2);
            echo "O resultado da multiplicação é: " . $resultado;
            break;
        case '/':
            $calculo = new Divisao();
            if ($n2 != 0) {
                $resultado = $calculo->calculo($n1, $n2);
                echo "O resultado da divisão é: " . $resultado;
            } else {
                echo "Não é possível dividir por 0";
            }
            break;
    }
}